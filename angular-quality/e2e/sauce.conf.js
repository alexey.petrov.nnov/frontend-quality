const {SpecReporter} = require('jasmine-spec-reporter');

const username = process.env.SAUCELABS_NAME;
const accessKey = process.env.SAUCELABS_KEY;

exports.config = {
  'seleniumAddress': "http://" + username + ":" + accessKey +
    "@ondemand.eu-central-1.saucelabs.com/wd/hub",
  baseUrl: 'http://localhost:4200/',

  'capabilities': {
    /* Pass Sauce User Name and Access Key */
    'username': username,
    'accessKey': accessKey,
    'browserName': 'Safari',
    'appiumVersion': '1.9.1',
    'deviceName': 'iPhone X Simulator',
    'deviceOrientation': 'portrait',
    'platformVersion': '11.0',
    'platformName': 'iOS',
  },


  allScriptsTimeout: 18000,
  specs: [
    './src/**/*.e2e-spec.ts'
  ],
  framework: 'jasmine',
  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 30000,
    print: function () {
    }
  },
  onPrepare() {
    require('ts-node').register({
      project: require('path').join(__dirname, './tsconfig.e2e.json')
    });
    jasmine.getEnv().addReporter(new SpecReporter({spec: {displayStacktrace: true}}));
  }
};
