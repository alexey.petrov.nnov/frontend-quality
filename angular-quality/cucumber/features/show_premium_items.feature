Feature: Show Premium Items
  Display the title

  Scenario: Home Page - Premium
    Given I am on the home page
    And I am "Hilde Hirsch" with ID "100000"
    When I click submit
    Then I should see Premium-Offers

  Scenario: Home Page - not Premium
    Given I am on the home page
    And I am "Martha Pfahl" with ID "1000"
    When I click submit
    Then I should not see Premium-Offers

